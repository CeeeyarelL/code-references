<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = ['name' , 'price' , 'description'];

    public function getRouteKeyName()
    {
    	return 'slug'; //Product::where('slug', $wildcard)->first();
    }
}
