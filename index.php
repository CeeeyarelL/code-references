@extends('layout.app')
@section('content')
      <div class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-12">
              <div class="card">
                <div class="card-header card-header-primary">
                  <h4 class="card-title ">Categories Table</h4>
                  @if (Session::has('success'))
                    <p class="card-category">
                      {{ Session::get('success') }}
                      {{ Session::put('success', null) }}
                    </p>
                  @endif                  
                </div>
                <div class="card-body">
                  <div class="table-responsive">
                    <table class="table">
                      <thead class=" text-primary">
                        <th>
                          Name
                        </th>
                        <th>
                          Description
                        </th>
                        <th>
                          Date Created
                        </th>
                        <th>
                          Action
                        </th>
                      </thead>
                      <tbody>
                      @foreach ($categories as $category)
                      <tr>
                        <td>
                          {{ $category->name }}
                        </td>
                        <td>
                          {{ $category->description }}
                        </td>
                        <td>
                          {{ $category->created_at->format('M d,Y h:i:s') }}
                        </td>
                        <td>
                          <a href="#" class="btn btn-primary">Edit</a>
                          <a href="#" class="btn btn-danger">Delete</a>
                        </td>
                      </tr>
                      @endforeach
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
@endsection      