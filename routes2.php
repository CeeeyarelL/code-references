<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PagesController@index');
Route::get('/about', 'PagesController@about');
Route::get('/services', 'PagesController@services');
// Route::get('/show/{id}', 'PagesController@show');
// Route::get('/create', 'PagesController@create');
// Route::post('/saveproduct', 'PagesController@saveproduct');
// Route::get('/edit/{id}', 'PagesController@editproduct');
// Route::post('/updateproduct', 'PagesController@updateproduct');
// Route::get('/delete/{id}', 'PagesController@deleteproduct');

//Products Controller

Route::get('/products', 'ProductsController@index');
Route::get('/products/create', 'ProductsController@create');
Route::post('/products', 'ProductsController@store');
Route::get('/products/{product}', 'ProductsController@show');
Route::get('/products/{product}/edit', 'ProductsController@edit');
Route::put('/products/{product}', 'ProductsController@update');
Route::get('products/{product}/delete', 'ProductsController@destroy');