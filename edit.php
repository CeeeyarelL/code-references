@extends('layouts.app')

@section('title')
    Edit Product
@endsection

@section('content')
    <div class="container">
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        {{-- {!! Form::open(['action' => 'ProductsController@update', 'method' => 'POST', 'class' => 'form-horizontal']) !!} --}}
        <form action="/products/{{$product->id}}" method="POST" class="form-horizontal">
            @csrf
            @method('PUT')
            {{-- {{Form::hidden('product', $product->id)}} --}}
            <div class="form-group">
                {{Form::label('name','Product Name')}}
                {{Form::text('name', $product->name, ['placeholder' => 'Product Name', 'class' => 'form-control'])}}
            </div>
            <div class="form-group">
                {{Form::label('price','Product Name')}}
                {{Form::number('price', $product->price , ['placeholder' => 'Product Price', 'class' => 'form-control'])}}
            </div>
            <div class="form-group">
                {{Form::label('description','Product Name')}}
                {{Form::textarea('description', $product->description , ['id' => 'editor','placeholder' => 'Product Description', 'class' => 'form-control'])}}
            </div>
                {{Form::submit('Update Product', ['class' => 'btn btn-primary'])}}
        {!! Form::close() !!}
          
    </div>
@endsection