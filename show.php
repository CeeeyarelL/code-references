@extends('layouts.app')

@section('title')
    Product
@endsection

@section('content')
    <div class="container">
        <h1>Product Details</h1>
        @if (Session::has('success'))
        <p class="alert alert-success">
            {{Session::get('success')}}
            {{Session::put('success',null)}}
        </p>
        @endif
            <h3>{{$product->name}}</h3>
            <h4>{{$product->price}}</h4>
            <h5>{!!$product->description!!}</h5>
            <hr>
            <small>Written in {{$product->created_at}}</small>
            <hr>
            <a href="/products/{{$product->id}}/edit" class="btn btn-default">Edit</a>
            <a href="/products/{{$product->id}}/delete" class="btn btn-danger">Delete</a>
    </div>
@endsection