@extends('layout.app')

@section('content')
      <div class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-12">
              <div class="card">
                <div class="card-header card-header-primary">
                  <h4 class="card-title">Create Category</h4>
                  @if ($errors->any())
                    <ul>
                      <p class="card-category">
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                      </p>                        
                    </ul>
                  @endif
                </div>
                <div class="card-body">

                  <form action="/categories/save" method="POST">
                    @csrf
                    <div class="row">
                      <div class="col-md-12">
                        <div class="form-group">
                          <label class="bmd-label-floating">Name</label>
                          <input 
                            type="text" 
                            class="form-control {{ $errors->has('name') ? 'is-danger' : '' }}" 
                            name="name"
                            value="{{ old('name') }}" 
                          >
                          <!-- <input type="text" class="form-control @error('name') is-danger @enderror" name="name"> -->
                          @if($errors->has('name'))
                            <p class="help is-danger">{{ $errors->first('name') }}</p>
                          @endif

                          <!-- @error('name')
                            <p class="help is-danger">{{ $errors->first('name') }}</p>
                          @enderror -->
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-12">
                        <div class="form-group">
                          <label>Description</label>
                          <div class="form-group">
                            <textarea class="form-control" rows="5" name="description"></textarea>
                          </div>
                        </div>
                      </div>
                    </div>
                    <button type="submit" class="btn btn-primary pull-right">Create</button>
                    <div class="clearfix"></div>
                  </form>

                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
@endsection
